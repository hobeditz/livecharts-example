﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiveCharts;
using LiveCharts.Defaults;

namespace Test_LiveCharts
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ChartValues<ScatterPoint> ChartValues { get; set; } = new ChartValues<ScatterPoint>();

        public MainWindow()
        {
            this.InitializeComponent();

            this.Chart.AnimationsSpeed = TimeSpan.FromMilliseconds(1000);
            this.Chart.AxisX[0].Unit = TimeSpan.TicksPerSecond;

            Timer timer = new Timer(100);
            List<ScatterPoint> cache = new List<ScatterPoint>();
            timer.Elapsed += (sender, args) =>
            {
                DateTime now = DateTime.Now;

                double x = now.Ticks;
                double y = new Random().NextDouble();

                cache.Add(new ScatterPoint(x, y));

                if (cache.Count > 1000 / timer.Interval)
                {
                    this.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        if (!this.Chart.AxisX.Any())
                        {
                            return;
                        }

                        this.Chart.AxisX[0].MinValue = now.Ticks - TimeSpan.FromMilliseconds(timer.Interval * 100).Ticks;
                        this.Chart.AxisX[0].MaxValue = now.Ticks;
                    }));

                    this.ChartValues.AddRange(cache);

                    cache.Clear();
                }
            };
            timer.Start();
        }
    }
}
